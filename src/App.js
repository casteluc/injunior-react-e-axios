import React from 'react';
import './App.css';

import Header from './pages/Header'
import Home from './pages/Home'
import Timeline from './pages/Timeline.jsx'
import { BrowserRouter, Switch, Route } from 'react-router-dom';

function App() {

  return (
    <BrowserRouter>
      <Header />

      <Switch>

        <Route exact path="/">
          <Home />
        </Route>

        <Route path="/timeline">
          <Timeline />
        </Route>

      </Switch>
    </BrowserRouter>
  )
}

export default App