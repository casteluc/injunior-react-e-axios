import React from 'react'
import './HomeButton.css'
import { Link } from 'react-router-dom'

export default props => {

    return (
        <Link to="/timeline" className="home-button"> Ir para a timeline </Link>
    )   
}