import React from 'react'
import './HomeText.css'

export default props => {

    return (
        <div className="home-text">
            <h2>Seja bem-vindo ao TwINter</h2>
            <h3>O lugar onde você pode fazer todos os seus testes usando React e Axios.</h3>
        </div>
    )
}