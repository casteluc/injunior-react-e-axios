import React from 'react'
import { Link } from 'react-router-dom'
import './HeaderNavegationBar.css'

export default props => {

    return (
        <nav className="header-navegation-bar">
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/timeline">Timeline</Link>
                </li>
            </ul>
        </nav>
    )
}