import React from 'react'
import './HeaderTitle.css'
import { Link } from 'react-router-dom'

export default props => {
    return (
        <Link to="/" className="header-title">
            <h1 >{props.title}</h1>
        </Link>
    )
}