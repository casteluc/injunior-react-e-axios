import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Post from './Post'


export default props => {
    const [posts, setPosts] = useState([])
    
    useEffect( () => {
        axios.get('https://treinamentoajax.herokuapp.com/messages')
            .then(response => {
                setPosts(response.data.reverse())
            })
    })

    return (
        <div className="posts-timeline">
            {posts.map( post => {
                return <Post postName={post.name} postMessage={post.message} />
            })}
        </div>
    )
}