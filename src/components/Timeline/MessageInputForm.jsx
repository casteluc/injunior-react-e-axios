import React, { useState } from 'react'
import axios from 'axios'

import './MessageInputForm.css'

export default props => {

    const [name, setName] = useState('')
    const [message, setMessage] = useState('')

    const handleInputChange = e => {
        if (e.target.type == "text") {
            setName(e.target.value)
        } else {
            setMessage(e.target.value)
        }
    }

    const handleSubmit = e => {
        e.preventDefault()

        setMessage('')

        let postBody = {
            "message": {
                "name": name,
                "message": message,
            }
        }

        axios.post('https://treinamentoajax.herokuapp.com/messages', postBody)
            .then( response => {
                console.log("Mensagem enviada")
            })

    }

    return (
        <form onSubmit={handleSubmit} className="message-input-form">

            <div>
                <label>Seu nome: </label>
                <input type="text" onChange={handleInputChange} value={name} />
            </div>
            <textarea cols="30" rows="10" placeholder="No que você está pensando?" onChange={handleInputChange} value={message}></textarea>

            <input type="submit" value="Postar"/>

        </form>
    )
}