import React from 'react'

import './Post.css'

export default props => {

    return (
        <div className="post">
            <h2>@{props.postName}</h2>
            <p>{props.postMessage}</p>
        </div>
    )
}