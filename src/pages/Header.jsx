import React from 'react'

import './Header.css'
import HeaderTitle from '../components/Header/HeaderTitle'
import HeaderNavegationBar from '../components/Header/HeaderNavegationBar'

export default props => {

    return (
        <header>
            <HeaderTitle title="TwINter"/>
            <HeaderNavegationBar />
        </header>
    )
}