import React from 'react'

import HomeText from '../components/Home/HomeText'
import HomeButton from '../components/Home/HomeButton'

import './Home.css'

export default props => {

    return (
        <div className="home">
            <HomeText />
            <HomeButton />
        </div>
    )
}