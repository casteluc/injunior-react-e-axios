import React from 'react'

import MessageInputForm from '../components/Timeline/MessageInputForm'
import PostsTimeline from '../components/Timeline/PostsTimeline'

import './Timeline.css'

export default props => {

    return (
        <div className="timeline">
            <MessageInputForm/>
            <PostsTimeline/>
        </div>
    )
}